module MkImg.Generate.ExprSpec (spec) where

import Test.Hspec
import MkImg.Data
import MkImg.Generate.Expr
import qualified MkImg.Generate as G
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.HashMap.Lazy as HM

spec :: Spec
spec = do
  describe "Evaluation expressions" $ do
    it "evaluates literal values to themselves" $ do
      evalExpr (MIILit 18) `shouldBe` 18

    it "evaluates arithmetic operations correctly" $ do
      let expr = (MIIDuOp Multiplication
                   (MIILit 2)
                   (MIILit 9))
       in evalExpr expr `shouldBe` 18

    it "evaluates bitwise operations correctly" $ do
      let expr = (MIIDuOp BitOr
                   (MIILit 4)
                   (MIILit 1))
       in evalExpr expr `shouldBe` 5

    it "evaluates boolean operations correctly" $ do
      let expr = (MIIDuOp BoolOr
                   (MIILit 3)
                   (MIILit 0))
       in evalExpr expr `shouldBe` 1

    it "evaluates comparisons correctly" $ do
      let expr = (MIIDuOp Equal
                   (MIILit 5)
                   (MIILit 5))
       in evalExpr expr `shouldBe` 1

    it "evaluates nested expressions correctly" $ do
      let expr = (MIIDuOp Subtraction
                   (MIIDuOp Addition
                     (MIILit 10)
                     (MIILit 5))
                   (MIIDuOp Multiplication
                     (MIILit 3)
                     (MIILit 2)))
       in evalExpr expr `shouldBe` 9

  describe "Evaluation of properties of chunks" $ do
    it "'present' works" $ do
      let r = (BL.unpack $ G.generate testEx) !! 0
       in r `shouldBe` 1

    it "'present_full' works" $ do
      let r = (BL.unpack $ G.generate testEx) !! 1
       in r `shouldBe` 0

    it "'size' works" $ do
      let r = (BL.unpack $ G.generate testEx) !! 2
       in r `shouldBe` 8

    it "'offset' works" $ do
      let r = (BL.unpack $ G.generate testEx) !! 3
       in r `shouldBe` 5

    it "'offset_in' works" $ do
      let r = (BL.unpack $ G.generate testEx) !! 4
       in r `shouldBe` 2


-- Complete example for testing of properties
testEx = HM.fromList [
    ("__image__",
     ChImage [
         prop "included" CPPresent
       , prop "includee" CPPresentFull
       , prop "filler" CPSize
       , ChTruncate (MIILit 4) [ ChCopy "included" ]
       ])
  , ("included",
     ChConcat [
         prop "filler" CPOffset
       , prop "filler" (CPOffsetIn "included")
       , ChCopy "filler"
       ])
  , ("filler",
     ChLiteral $ RDByteString $ BC.pack "AAAAAAAA")
  ]

prop n p = ChLiteral $ RDNum [ MIRawInt (MIIChProp n p) 1 LittleEndian False ]

