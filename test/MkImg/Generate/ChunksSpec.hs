module MkImg.Generate.ChunksSpec (spec) where

import Test.Hspec
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.HashMap.Lazy as HM

import MkImg.Data
import MkImg.Generate.Chunks
import qualified MkImg.Generate as G

spec :: Spec
spec = do
  describe "Generation of chunks" $ do
    it "IMAGE works" $ do
      let bs = BC.pack "This! is! DATA!"
          ch = ChImage [ ChLiteral $ RDByteString bs ]
       in tGenChunk HM.empty ch `shouldBe` bs

    it "CONCAT works" $ do
      let ch = ChConcat
                 [ ChLiteral $ RDByteString $ BC.pack "This! "
                 , ChLiteral $ RDByteString $ BC.pack "Is! "
                 , ChLiteral $ RDByteString $ BC.pack "DATA!" ]
       in tGenChunk HM.empty ch `shouldBe` BC.pack "This! Is! DATA!"

    it "LITERAL works" $ do
      let bs = BC.pack "This! Is! DATA!"
          ch = ChLiteral $ RDByteString bs
       in tGenChunk HM.empty ch `shouldBe` bs

 {- it "READFILE works" $ -- UNTESTABLE ?
      pendingWith "Figure out how to test this" -}

    it "TRUNCATE works" $ do
      let bs = BC.pack "ABCDEFGHIJK"
          ch = ChTruncate (MIILit 4) [ ChLiteral $ RDByteString bs ]
       in tGenChunk HM.empty ch `shouldBe` BL.take 4 bs

    it "COPY works" $ do
      let bs = BC.pack "This! Is! DATA!"
          chInner = ChLiteral $ RDByteString bs
          chOuter = ChImage [ ChCopy "inner" ]
          sp = HM.fromList [ ("__image__", chOuter)
                           , ("inner", chInner) ]
       in G.generate sp `shouldBe` bs

    it "IF works" $ do
      let bsThen = BC.pack "THEN"
          bsElse = BC.pack "ELSE"
          ch = ChIf (MIILit 0) 
                 (ChLiteral $ RDByteString bsThen)
                 (ChLiteral $ RDByteString bsElse)
       in tGenChunk HM.empty ch `shouldBe` bsElse

