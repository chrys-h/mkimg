module MkImg.Generate.RawDataSpec (spec) where

import Test.Hspec
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.HashMap.Lazy as HM

import MkImg.Data
import qualified MkImg.Generate.RawData as RD

spec :: Spec
spec = do
  describe "Raw data specified as bytestrings" $ do
    it "is pasted in place" $ do
      let bs = BC.pack "This! is! DATA!"
       in RD.tGenerate HM.empty (RDByteString bs) `shouldBe` bs

  describe "Raw integers" $ do
    it "are generated with the correct length" $ do
      let ri = MIRawInt (MIILit 22) 2 LittleEndian False
       in (length $ BL.unpack $ RD.tGenerate HM.empty $ RDNum [ri]) `shouldBe` 2

    it "are generated with the correct byte order" $ do
      let ri = MIRawInt (MIILit 22) 2 BigEndian False
          bs = RD.tGenerate HM.empty $ RDNum [ri]
       in (BL.unpack bs !! 0) `shouldBe` 0

  describe "Integers specified as formulae" $ do
    it "are evaluated correctly" $ do
      let exp = (MIIDuOp Addition
                  (MIILit 3)
                  (MIILit 2))
          ri = MIRawInt exp 8 LittleEndian False
          bs = RD.tGenerate HM.empty $ RDNum [ri]
       in (BL.unpack bs !! 0) `shouldBe` 5

