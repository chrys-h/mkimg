{-# LANGUAGE OverloadedStrings #-}

module MkImg.Parse.ExprSpec (spec) where

import Test.Hspec
import MkImg.Parse.Expr (parse)
import MkImg.Data

spec :: Spec
spec = do
  describe "Parsing of operators" $ do
    it "parses binary operators correctly" $ do
      let txt = "1 + 3"
          expect = MIIDuOp Addition (MIILit 1) (MIILit 3)
       in parse txt `shouldBe` expect

    it "parses unary operators correctly" $ do
      let txt = "~5"
          expect = MIIUnOp BitNeg (MIILit 5)
       in parse txt `shouldBe` expect

    context "operator precedence" $ do
      it "unary operators over binary operators" $ do
        let txt = "~5 + 8"
            expect = MIIDuOp Addition
                       (MIIUnOp BitNeg (MIILit 5))
                       (MIILit 8)
         in parse txt `shouldBe` expect
 
      it "** over * / %" $ do
        let txt = "10 ** 5 * 2"
            expect = MIIDuOp Multiplication
                       (MIIDuOp Exponentiation 
                          (MIILit 10)
                          (MIILit 5))
                       (MIILit 2)
         in parse txt `shouldBe` expect
 
      it "* / % over + -" $ do
        let txt = "2 + 5 % 3"
            expect = MIIDuOp Addition
                       (MIILit 2)
                       (MIIDuOp Modulus
                          (MIILit 5)
                          (MIILit 3))
         in parse txt `shouldBe` expect
 
      it "+ - over << >>" $ do
        let txt = "1 << 3 + 1"
            expect = MIIDuOp BitShiftL
                       (MIILit 1)
                       (MIIDuOp Addition 
                          (MIILit 3)
                          (MIILit 1))
         in parse txt `shouldBe` expect
 
      it "<< >> over < > <= >=" $ do
        let txt = "2 > 1 << 3"
            expect = MIIDuOp Greater
                       (MIILit 2)
                       (MIIDuOp BitShiftL
                          (MIILit 1)
                          (MIILit 3))
         in parse txt `shouldBe` expect
 
      it "< > <= >= over == !=" $ do
        let txt = "3 >= 2 != 0"
            expect = MIIDuOp NotEqual
                       (MIIDuOp GreaterEq
                          (MIILit 3)
                          (MIILit 2))
                       (MIILit 0)
         in parse txt `shouldBe` expect
 
      it "== != over &" $ do
        let txt = "1 & 2 == 3"
            expect = MIIDuOp BitAnd
                       (MIILit 1)
                       (MIIDuOp Equal
                          (MIILit 2)
                          (MIILit 3))
         in parse txt `shouldBe` expect
 
      it "& over ^" $ do
        let txt = "2 ^ 3 & 4"
            expect = MIIDuOp BitXor
                       (MIILit 2)
                       (MIIDuOp BitAnd
                          (MIILit 3)
                          (MIILit 4))
         in parse txt `shouldBe` expect
 
      it "^ over |" $ do
        let txt = "3 ^ 2 | 5"
            expect = MIIDuOp BitOr
                       (MIIDuOp BitXor
                          (MIILit 3)
                          (MIILit 2))
                       (MIILit 5)
         in parse txt `shouldBe` expect
 
      it "| over &&" $ do
        let txt = "2 | 1 && 0"
            expect = MIIDuOp BoolAnd
                       (MIIDuOp BitOr
                          (MIILit 2)
                          (MIILit 1))
                       (MIILit 0)
         in parse txt `shouldBe` expect
 
      it "&& over ||" $ do
        let txt = "1 && 0 || 1"
            expect = MIIDuOp BoolOr
                       (MIIDuOp BoolAnd
                          (MIILit 1)
                          (MIILit 0))
                       (MIILit 1)
         in parse txt `shouldBe` expect
 
    it "binary operators are left-associative" $ do
      let txt = "1 + 2 - 3"
          expect = MIIDuOp Subtraction
                     (MIIDuOp Addition
                        (MIILit 1)
                        (MIILit 2))
                     (MIILit 3)
       in parse txt `shouldBe` expect

  describe "Parsing of properties of chunks" $ do
    it "recognizes chunk properties" $ do
      let txt = "some_chunk.size"
          expect = MIIChProp "some_chunk" CPSize
       in parse txt `shouldBe` expect

    it "accepts labels in quotation marks" $ do
      let txt = "\"some chunk\".size"
          expect = MIIChProp "some chunk" CPSize
       in parse txt `shouldBe` expect

    it "recognizes properties with arguments" $ do
      let txt = "some_chunk.offset_in(parent)"
          expect = MIIChProp "some_chunk" (CPOffsetIn "parent")
       in parse txt `shouldBe` expect

    it "considers properties as atoms" $ do
      let txt = "some_chunk.size + 5"
          expect = MIIDuOp Addition
                     (MIIChProp "some_chunk" CPSize)
                     (MIILit 5)
       in parse txt `shouldBe` expect

  describe "General" $ do
    it "considers expressions in parentheses as atoms" $ do
      let txt = "2 | (1 + 0)"
          expect = MIIDuOp BitOr
                     (MIILit 2)
                     (MIIDuOp Addition
                        (MIILit 1)
                        (MIILit 0))
       in parse txt `shouldBe` expect
      
    it "ignores whitespace" $ do
      let txt = "    2| ( 1 +   0)  "
          expect = MIIDuOp BitOr
                     (MIILit 2)
                     (MIIDuOp Addition
                        (MIILit 1)
                        (MIILit 0))
       in parse txt `shouldBe` expect
      


