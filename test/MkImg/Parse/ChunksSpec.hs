{-# LANGUAGE OverloadedStrings #-}

module MkImg.Parse.ChunksSpec (spec) where

import qualified Data.Text as T
import qualified Data.HashMap.Lazy as HM
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Base64.Lazy as BL64
import qualified Data.ByteString.Lazy.Char8 as BC
import Test.Hspec
import MkImg.Parse.Chunks (parse, parseChunk)
import MkImg.Data
import Control.Exception (evaluate)

spec :: Spec
spec = do
  describe "Parsing of basic chunks" $ do
    it "parses empty chunks with no arguments" $ do
      let txt = "IMAGE {}"
          expect = ChImage []
       in parseChunk txt `shouldBe` expect

    it "parses nested chunks" $ do
      let txt = "IMAGE { CONCAT {} LITERAL {} }"
          expect = ChImage [ ChConcat [], ChLiteral (RDNum []) ]
       in parseChunk txt `shouldBe` expect

  describe "Parsing of chunks with properties" $ do
    it "parses chunks with properties" $ do
      let txt = "TRUNCATE (size: 64) { }"
          expect = ChTruncate (MIILit 64) []
       in parseChunk txt `shouldBe` expect

    it "fails if the wrong properties are given" $ do
      let txt = "TRUNCATE (path: \"file\") { }"
       in evaluate (parse txt) `shouldThrow` anyException

  describe "Parsing of raw data" $ do
    it "parses square brackets for literal chunk" $ do
      let txt = "[ 55 AA ]"
          expect = ChLiteral $ RDNum [ MIRawInt (MIILit 85) 1 LittleEndian False
                                     , MIRawInt (MIILit 170) 1 LittleEndian False ]
       in parseChunk txt `shouldBe` expect

    it "parses raw data as hex bytes by default" $ do
      let txt = "LITERAL { 55 AA }"
          expect = ChLiteral $ RDNum $ [ MIRawInt (MIILit 85) 1 LittleEndian False
                                       , MIRawInt (MIILit 170) 1 LittleEndian False ]
       in parseChunk txt `shouldBe` expect

    it "parses raw integers" $ do
      let txt = "[ 0d125Lu ]"
          expect = ChLiteral $ RDNum [ MIRawInt (MIILit 125) 4 LittleEndian False ]
       in parseChunk txt `shouldBe` expect

    it "parses expressions in raw integers" $ do
      let txt = "[ (3 + 1)Lu ]"
          expr = MIIDuOp Addition (MIILit 3) (MIILit 1)
          expect = ChLiteral $ RDNum [ MIRawInt expr 4 LittleEndian False ]
       in parseChunk txt `shouldBe` expect

    it "parses text in raw data" $ do
      let txt = "LITERAL { \"AB\" }"
          expect = ChLiteral $ RDByteString $ BC.pack "AB"
       in parseChunk txt `shouldBe` expect

    it "parses base64 raw data" $ do
      let txt = "[[ 3rkueTnA ]]"
          expect = ChLiteral $ RDByteString $ BL64.decodeLenient $ BC.pack "3rkueTnA"
       in parseChunk txt `shouldBe` expect

  describe "Parsing of labels" $ do
    it "adds labels to the labels dictionary" $ do
      let expect = HM.fromList [ ("__image__", ChImage [])
                               , ("label", ChConcat []) ]
       in parse simpleLabelTest `shouldBe` expect

    it "replaces non-toplevel labeled chunks with copy chunks" $ do
      let imageChunk = ChImage [ ChCopy "inside" ]
          expect = HM.fromList [ ("__image__", imageChunk)
                               , ("inside", ChLiteral (RDNum []))
                               , ("label", ChConcat []) ]
       in parse complexLabelTest `shouldBe` expect

  describe "Specific chunks" $ do
    it "parses IMAGE chunks" $ do
      let txt = "IMAGE {}"
          expect = ChImage []
       in parseChunk txt `shouldBe` expect

    it "parses CONCAT chunks" $ do
      let txt = "CONCAT {}"
          expect = ChConcat []
       in parseChunk txt `shouldBe` expect

    it "parses LITERAL chunks" $ do
      let txt = "LITERAL {}"
          expect = ChLiteral (RDNum [])
       in parseChunk txt `shouldBe` expect

    it "parses READFILE chunks" $ do
      let txt = "READFILE(path: \"some_file\")"
          expect = ChReadFile "some_file"
       in parseChunk txt `shouldBe` expect

    it "parses TRUNCATE chunks" $ do
      let txt = "TRUNCATE (size: 25) {}"
          expect = ChTruncate (MIILit 25) []
       in parseChunk txt `shouldBe` expect

    it "parses COPY chunks" $ do
      let txt = "COPY(ref: \"some_chunk\")"
          expect = ChCopy "some_chunk"
       in parseChunk txt `shouldBe` expect

    it "parses IF chunks" $ do
      let txt = "IF (c: 1) { CONCAT {} }"
          expect = ChIf (MIILit 1) (ChConcat []) ChEmpty
       in parseChunk txt `shouldBe` expect

  describe "Data size parsing" $ do
    it "parses integer data sizes" $ do
      let txt = "TRUNCATE (size: 25 kB) { }"
          formula = MIIDuOp Multiplication (MIILit 1000) (MIILit 25)
          expect = ChTruncate formula []
       in parseChunk txt `shouldBe` expect

    it "parses formula data sizes" $ do
      let txt = "TRUNCATE (size: (5 + 25) kB) { }"
          formula = MIIDuOp Multiplication (MIILit 1000) (MIIDuOp Addition (MIILit 5) (MIILit 25))
          expect = ChTruncate formula []
       in parseChunk txt `shouldBe` expect

    it "defaults to bytes" $ do
      let txt = "TRUNCATE (size: 25) { }"
          expect = ChTruncate (MIILit 25) []
       in parseChunk txt `shouldBe` expect

  describe "General" $ do
    it "ignores whitespace" $ do
      let txt = "  IMAGE{        CONCAT{}}  "
          expect = ChImage [ ChConcat [] ]
       in parseChunk txt `shouldBe` expect

    it "ignores comments" $ do
      let txt = "IMAGE {} # No comment."
          expect = ChImage []
       in parseChunk txt `shouldBe` expect

simpleLabelTest = unlines [
    "IMAGE {}",
    "\"label\" = CONCAT { }"
  ]

complexLabelTest = unlines [
    "IMAGE {",
    "    \"inside\" = LITERAL {}",
    "}",
    "\"label\" = CONCAT { }"
  ]


