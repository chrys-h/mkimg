{-# LANGUAGE FlexibleContexts #-}

module MkImg.Generate (generate) where

import qualified Data.HashMap.Lazy as HM
import Data.HashMap.Lazy ((!))
import qualified Data.ByteString.Lazy as BL
import Control.Monad.Reader
import Control.Applicative ((<$>), (<*>))

import MkImg.Data
import MkImg.Generate.Chunks
import MkImg.Generate.Common
import MkImg.Generate.Properties

generate :: ImageSpec -> BL.ByteString
generate spec = chpRes $ prods ! "__image__"
  where prods = HM.map (computeProds prods) $ spec

computeProds :: ImageProds -> Chunk -> ChunkProds
computeProds ps ch = runReader (go ch) ps
  where go :: (MonadReader ImageProds mr) => Chunk -> mr ChunkProds
        go c = ChunkProds <$> genChunk c
                          <*> computeSize c
                          <*> computeOffset c
                          <*> checkPresent c
                          <*> checkPresentFull c

