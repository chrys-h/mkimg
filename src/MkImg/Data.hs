module MkImg.Data where

import qualified Data.HashMap.Lazy as HM
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as BL

type ChunkLabel = String

data ChunkProp = CPPresent
               | CPPresentFull
               | CPSize 
               | CPOffset 
               | CPOffsetIn ChunkLabel
               deriving (Show, Read, Eq)

data MIUnOp = ArithNeg
            | BoolNeg
            | BitNeg
            deriving (Show, Read, Eq, Enum, Bounded)

data MIDuOp = Addition
            | Subtraction
            | Multiplication
            | Division
            | Modulus
            | BitAnd
            | BitOr
            | BitXor
            | BoolAnd
            | BoolOr
            | BitShiftR
            | BitShiftL
            | Exponentiation
            | Equal
            | NotEqual
            | Lesser
            | Greater
            | LesserEq
            | GreaterEq
            deriving (Show, Read, Eq, Enum, Bounded)

data MIInt = MIILit Int
           | MIIChProp ChunkLabel ChunkProp
           | MIIUnOp MIUnOp MIInt
           | MIIDuOp MIDuOp MIInt MIInt
           deriving (Show, Read, Eq)

data MIEndianness = BigEndian | LittleEndian
                  deriving (Show, Read, Eq, Enum, Bounded)

data MIRawInt = MIRawInt
              { miriValue      :: MIInt
              , miriLength     :: Int -- in bytes
              , miriEndianness :: MIEndianness
              , miriSigned     :: Bool
              } deriving (Show, Read, Eq)

data RawData = RDNum [MIRawInt]
             | RDByteString BL.ByteString
             deriving (Show, Read, Eq)

data Chunk = ChImage    [Chunk]
           | ChConcat   [Chunk]
           | ChLiteral  RawData
           | ChReadFile String
           | ChTruncate MIInt [Chunk]
           | ChCopy     ChunkLabel
           | ChIf       MIInt Chunk Chunk
           | ChEmpty
           deriving (Show, Read, Eq)

type ImageSpec = HM.HashMap ChunkLabel Chunk


