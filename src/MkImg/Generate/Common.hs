{-# LANGUAGE FlexibleContexts #-}

module MkImg.Generate.Common where

import qualified Data.ByteString.Lazy as BL
import qualified Data.HashMap.Lazy as HM
import Data.HashMap.Lazy ((!))
import Control.Monad.Reader
import MkImg.Data

-- The products of a chunk are all the data derived from it:
--   - The resulting ByteString
--   - Its length
--   - Its position
--   - All of its properties
data ChunkProds = ChunkProds
                { chpRes :: BL.ByteString
                , chpSize :: Int
                , chpOffset :: Int
                , chpPresent :: Bool
                , chpPresentFull :: Bool
                }

type ImageProds = HM.HashMap ChunkLabel ChunkProds


-- Get products of chunks from the Reader Monad
getChRes :: (MonadReader ImageProds mr) => ChunkLabel -> mr BL.ByteString
getChRes n = asks (chpRes . (! n))

getChSize :: (MonadReader ImageProds mr) => ChunkLabel -> mr Int
getChSize n = asks (chpSize . (! n))

getChOffset :: (MonadReader ImageProds mr) => ChunkLabel -> mr Int
getChOffset n = asks (chpOffset . (! n))

getChOffsetIn :: (MonadReader ImageProds mr) => ChunkLabel -> ChunkLabel -> mr Int
getChOffsetIn n c = undefined

getChPresent :: (MonadReader ImageProds mr) => ChunkLabel -> mr Bool
getChPresent n = asks (chpPresent . (! n))

getChPresentFull :: (MonadReader ImageProds mr) => ChunkLabel -> mr Bool
getChPresentFull n = asks (chpPresentFull . (! n))


