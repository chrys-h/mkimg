{-# LANGUAGE FlexibleContexts #-}

module MkImg.Generate.RawData (generate, tGenerate) where

import qualified Data.ByteString.Lazy as BL
import Data.Binary.Put
import MkImg.Generate.Common
import Control.Monad.Reader
import Control.Monad.Trans.Reader
import Control.Monad.Reader.Class

import MkImg.Data
import MkImg.Generate.Expr

-- Only for the unit tests
tGenerate :: ImageProds -> RawData -> BL.ByteString
tGenerate ps rd = runReader (generate rd) ps

generate :: (MonadReader ImageProds mr) => RawData -> mr BL.ByteString
generate (RDByteString bs) = return bs
generate (RDNum        is) = BL.concat <$> mapM genI is
                     -- FIXME: Do this in a more efficient way ?

genI :: (MonadReader ImageProds mr) => MIRawInt -> mr BL.ByteString
genI (MIRawInt v l e s) = runPut . putR l e s <$> eval v
  where
    putR 1 _            False = putWord8 . fromIntegral 
    putR 1 _            True  = putInt8 . fromIntegral 
    putR 2 BigEndian    False = putWord16be . fromIntegral 
    putR 2 LittleEndian False = putWord16le . fromIntegral 
    putR 2 BigEndian    True  = putInt16be . fromIntegral 
    putR 2 LittleEndian True  = putInt16le . fromIntegral 
    putR 4 BigEndian    False = putWord32be . fromIntegral 
    putR 4 LittleEndian False = putWord32le . fromIntegral 
    putR 4 BigEndian    True  = putInt32be . fromIntegral 
    putR 4 LittleEndian True  = putInt32le . fromIntegral 
    putR 8 BigEndian    False = putWord64be . fromIntegral 
    putR 8 LittleEndian False = putWord64le . fromIntegral 
    putR 8 BigEndian    True  = putInt64be . fromIntegral 
    putR 8 LittleEndian True  = putInt64le . fromIntegral 
    putR 16 _ _ = error "long long long is too long for mkimg"


