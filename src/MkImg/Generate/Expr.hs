{-# LANGUAGE FlexibleContexts #-}

module MkImg.Generate.Expr (eval, evalExpr) where

import qualified Data.Bits as B
import qualified Data.HashMap.Lazy as HM
import Control.Monad.Reader
import Control.Monad.Reader.Class
import Control.Applicative ((<$>), (<*>))
import MkImg.Generate.Common
import MkImg.Data

evalExpr :: MIInt -> Int
evalExpr e = runReader (eval e) HM.empty

eval :: (MonadReader ImageProds mr) => MIInt -> mr Int
eval (MIILit x)       = return x
eval (MIIChProp c p)  = undefined
eval (MIIUnOp op x)   = f <$> eval x
  where f = unOpFn op
eval (MIIDuOp op x y) = f <$> eval x <*> eval y
  where f = duOpFn op

unOpFn :: MIUnOp -> Int -> Int
unOpFn ArithNeg = negate
unOpFn BoolNeg  = b2i . not . i2b
unOpFn BitNeg   = B.complement

duOpFn :: MIDuOp -> Int -> Int -> Int
duOpFn Addition       x y = x + y
duOpFn Subtraction    x y = x - y
duOpFn Multiplication x y = x * y
duOpFn Division       x y = x `div` y
duOpFn Modulus        x y = x `mod` y
duOpFn BitAnd         x y = x B..&. y
duOpFn BitOr          x y = x B..|. y
duOpFn BitXor         x y = x `B.xor` y
duOpFn BoolAnd        x y = b2i $ (i2b x) && (i2b y)
duOpFn BoolOr         x y = b2i $ (i2b x) || (i2b y)
duOpFn BitShiftR      x y = x `B.shiftR` y
duOpFn BitShiftL      x y = x `B.shiftL` y
duOpFn Exponentiation x y = x ^ y
duOpFn Equal          x y = b2i $ x == y
duOpFn NotEqual       x y = b2i $ x /= y
duOpFn Lesser         x y = b2i $ x < y
duOpFn Greater        x y = b2i $ x > y
duOpFn LesserEq       x y = b2i $ x <= y
duOpFn GreaterEq      x y = b2i $ x >= y

b2i :: Bool -> Int
b2i True = 1
b2i False = 0

i2b :: Int -> Bool
i2b = (/= 0)
 
