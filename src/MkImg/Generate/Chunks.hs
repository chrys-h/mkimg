{-# LANGUAGE FlexibleContexts #-}

module MkImg.Generate.Chunks (genChunk, tGenChunk) where

import qualified Data.HashMap.Lazy as HM
import Data.HashMap.Lazy ((!))
import qualified Data.ByteString.Lazy as BL
import MkImg.Data
import MkImg.Generate.Common
import Control.Monad.Reader
import Control.Monad.Reader.Class
import qualified MkImg.Generate.RawData as RD
import MkImg.Generate.Expr

-- Simply generate a chunk ; used for the tests
tGenChunk :: ImageProds -> Chunk -> BL.ByteString
tGenChunk ps ch = runReader (genChunk ch) ps

genChunk :: (MonadReader ImageProds mr) => Chunk -> mr BL.ByteString
genChunk (ChImage    cts         ) = concatCts cts
genChunk (ChConcat   cts         ) = concatCts cts
genChunk (ChLiteral  dat         ) = RD.generate dat
genChunk (ChReadFile path        ) = undefined
genChunk (ChTruncate size cts    ) = genTruncate size cts
genChunk (ChCopy     lbl         ) = getChRes lbl
genChunk (ChIf       cnd  ifT ifF) = genIf cnd ifT ifF
genChunk (ChEmpty                ) = return BL.empty

concatCts :: (MonadReader ImageProds mr) => [Chunk] -> mr BL.ByteString
concatCts cts = BL.concat <$> mapM genChunk cts

genTruncate :: (MonadReader ImageProds mr) => MIInt -> [Chunk] -> mr BL.ByteString
genTruncate s cts = BL.take <$> (fromIntegral <$> eval s) <*> concatCts cts

genIf :: (MonadReader ImageProds mr) => MIInt -> Chunk -> Chunk -> mr BL.ByteString
genIf c chThen chElse = do
  cond <- eval c
  if cond == 0
    then genChunk chElse
    else genChunk chThen

