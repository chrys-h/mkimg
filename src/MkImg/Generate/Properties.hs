{-# LANGUAGE FlexibleContexts #-}

module MkImg.Generate.Properties where

import qualified Data.HashMap.Lazy as HM
import Data.HashMap.Lazy ((!))
import qualified Data.ByteString.Lazy as BL
import Control.Monad.Reader
import Control.Monad.Reader.Class
import Control.Applicative ((<$>), (<*>))
import MkImg.Data
import MkImg.Generate.Common

computeSize :: (MonadReader ImageProds mr) => Chunk -> mr Int
computeSize = undefined

computeOffset :: (MonadReader ImageProds mr) => Chunk -> mr Int
computeOffset = undefined

checkPresent :: (MonadReader ImageProds mr) => Chunk -> mr Bool
checkPresent = undefined

checkPresentFull :: (MonadReader ImageProds mr) => Chunk -> mr Bool
checkPresentFull = undefined


