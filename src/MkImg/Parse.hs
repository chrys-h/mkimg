module MkImg.Parse (parse) where

import qualified MkImg.Parse.Chunks as Chunks
import MkImg.Data

parse :: String -> ImageSpec
parse = Chunks.parse


