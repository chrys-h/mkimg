{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}

module MkImg.Parse.Expr (MkImg.Parse.Expr.parse, expr) where

import Control.Applicative ((<$>), (<*>), (<*), (*>))
import Control.Exception (throw, Exception)
import Text.Parsec
import Text.Parsec.Expr
import qualified Text.Parsec.Token as PT

import MkImg.Parse.Common
import MkImg.Data

instance Exception ParseError

parse :: String -> MIInt
parse t = case Text.Parsec.parse expr "" t of
  Left err -> throw err
  Right r -> r

expr :: Parsec String u MIInt
expr = buildExpressionParser ops term

chunkProp = MIIChProp <$> label <* char '.' <*> prop
  where 
    label = identifier 
        <|> (between (char '"') (char '"') $ many1 $ noneOf "\"")
    prop = choice [
        reserved "present" >> return CPPresent
      , reserved "present_full" >> return CPPresentFull
      , reserved "size" >> return CPSize
      , reserved "offset" >> return CPOffset
      , reserved "offset_in" >> CPOffsetIn <$> parens label
      ]

term = spaces *> t <* spaces
  where t = parens expr
         <|> MIILit . fromIntegral <$> natural
         <|> chunkProp

binOp  n f = Infix  (reservedOp n >> return f) AssocLeft
prefix n f = Prefix (reservedOp n >> return f)

ops = [ [ prefix "-" (MIIUnOp ArithNeg)
        , prefix "!" (MIIUnOp BoolNeg)
        , prefix "~" (MIIUnOp BitNeg)         ]
      , [ binOp "**" (MIIDuOp Exponentiation) ]
      , [ binOp "*"  (MIIDuOp Multiplication)
        , binOp "/"  (MIIDuOp Division)
        , binOp "%"  (MIIDuOp Modulus)        ]
      , [ binOp "+"  (MIIDuOp Addition)
        , binOp "-"  (MIIDuOp Subtraction)    ]
      , [ binOp "<<" (MIIDuOp BitShiftL)
        , binOp ">>" (MIIDuOp BitShiftR)      ]
      , [ binOp "<"  (MIIDuOp Lesser)
        , binOp ">"  (MIIDuOp Greater)
        , binOp "<=" (MIIDuOp LesserEq)
        , binOp ">=" (MIIDuOp GreaterEq)      ]
      , [ binOp "==" (MIIDuOp Equal)
        , binOp "!=" (MIIDuOp NotEqual)       ]
      , [ binOp "&"  (MIIDuOp BitAnd)         ]
      , [ binOp "^"  (MIIDuOp BitXor)         ]
      , [ binOp "|"  (MIIDuOp BitOr)          ]
      , [ binOp "&&" (MIIDuOp BoolAnd)        ]
      , [ binOp "||" (MIIDuOp BoolOr)         ] ]


