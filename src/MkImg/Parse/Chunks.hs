module MkImg.Parse.Chunks (MkImg.Parse.Chunks.parse, parseChunk, parser) where

import Control.Exception (throw)
import Text.Parsec
import qualified Data.HashMap.Lazy as HM
import Control.Applicative ((<$>), (<*>), (<*), (*>))
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Base64.Lazy as BL64
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Text.Parsec.Number as PN

import qualified MkImg.Parse.Expr as Expr
import MkImg.Data
import MkImg.Parse.Common

parse :: String -> ImageSpec
parse t = case runParser parser HM.empty "" t of
  Left err -> throw err
  Right r -> r

parseChunk :: String -> Chunk
parseChunk t = case runParser chunkMaybeLabelled HM.empty "" t of
  Left err -> throw err
  Right r -> r

parser :: Parsec String ImageSpec ImageSpec
parser = many1 chunkMaybeLabelled >> getState

-- Parses a chunk that may have a label
chunkMaybeLabelled = spaces *> (chunkLabelled <|> chunk)

chunkLabelled = do
  lbl <- char '"' *> identifier <* char '"'
  spaces >> char '=' >> spaces
  ch <- chunk
  modifyState (HM.insert lbl ch)
  return $ ChCopy lbl

chunk :: Parsec String ImageSpec Chunk
chunk = chImage
    <|> chConcat
    <|> chLiteral
    <|> chLiteralBrackets
    <|> chReadFile
    <|> chTruncate
    <|> chCopy
    <|> chIf

chImage = do
  ch <- reserved "IMAGE" >> ChImage <$> chunkContents
  modifyState (HM.insert "__image__" ch)
  return ch

chConcat = reserved "CONCAT" >> ChConcat <$> chunkContents

chLiteral = reserved "LITERAL" >> ChLiteral <$> braces rawData

chLiteralBrackets = ChLiteral <$> brackets rawData

quotedStr = spaces *> char '"' *> (many $ noneOf "\"") <* char '"' <* spaces

chReadFile = reserved "READFILE"
           >> ChReadFile <$> (startProps
                              *> prop "path" quotedStr
                             <*  stopProps)

chTruncate = reserved "TRUNCATE" 
           >> ChTruncate <$> (startProps
                              *> prop "size" dataSize
                             <*  stopProps)
                         <*> chunkContents

chCopy = reserved "COPY"
       >> ChCopy <$> (startProps
                      *> prop "ref" (identifier <|> quotedStr)
                     <*  stopProps)

chIf = reserved "IF"
     >> ChIf <$> (startProps
                  *> prop "c" Expr.expr
                 <*  stopProps)
             <*  char '{'
             <*> chunkMaybeLabelled
             <*> option ChEmpty chunkMaybeLabelled
             <*  char '}'

chunkContents = braces $ many $ chunkMaybeLabelled


-- Chunk properties machinery
startProps = spaces >> char '(' >> spaces

stopProps = spaces >> char ')' >> spaces

prop n p = reserved n *> spaces *> char ':' *> spaces *> p <* optional (char ',')


-- Data size specification
dataSize = do
  v <- Expr.expr
  spaces
  f <- dataUnit
  return $ f v

dataUnit = choice [ u "kB" 1000
                  , u "MB" (1000 ^ 2)
                  , u "GB" (1000 ^ 3)
                  , u "TB" (1000 ^ 4)
                  , u "kiB" 1024
                  , u "MiB" (1024 ^ 2)
                  , u "GiB" (1024 ^ 3)
                  , u "TiB" (1024 ^ 4)
                  , return (\x -> x) ]
  where u n v = reserved n >> return (MIIDuOp Multiplication (MIILit v))


-- Raw data parsing
rawData = brackets rawDatB64
      <|> rawDatString
      <|> rawDatNum

-- Raw data in the form of a sequence of integers
rawDatNum = spaces *> (RDNum <$> many (rawInt <* spaces) )

rawInt = MIRawInt <$> v <*> l <*> e <*> s
  where
    v = specInt <|> parens Expr.expr
    l = option 1 lStr
    lStr = choice [ try (string "LLL" >> return 16)
                  , try (string "LL" >> return 8)
                  , try (string "L" >> return 4)
                  , try (string "S" >> return 2)
                  , try (string "O" >> return 1) ]
    e = option LittleEndian eStr
    eStr = choice [ try (string "l" >> return LittleEndian)
                  , try (string "m" >> return BigEndian) ]
    s = option False sStr
    sStr = choice [ try (string "s" >> return True)
                  , try (string "u" >> return False) ]

specInt = MIILit <$> choice prefs
  where prefs = [ try (string "0x" >> PN.hexnum)
                , try (string "0d" >> PN.decimal)
                , try (string "0b" >> PN.binary)
                ,                     PN.hexnum ]

-- Raw data in the form of a string of text
rawDatString = RDByteString . BC.pack <$> quotedStr

-- Raw data in the form of base64 data
rawDatB64 = RDByteString . BL64.decodeLenient . BC.pack <$> many (noneOf "]")


