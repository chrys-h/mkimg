{-# LANGUAGE OverloadedStrings #-}

module MkImg.Parse.Common where

import Control.Applicative ((<$>), (<*>), (<*), (*>))
import Control.Exception (throw, Exception)
import Text.Parsec
import Text.Parsec.Language
import qualified Text.Parsec.Token as PT

-- Lexer definition

opNames = [ "-" , "!" , "~"
          , "**", "*" , "/" , "%"
          , "+" , "-"
          , "<<", ">>"
          , "<" , ">" , "<=", ">="
          , "==", "!="
          , "&" , "^" , "|"
          , "&&", "||"
          ]

chunkNames = [ "IMAGE"
             , "CONCAT"
             , "LITERAL"
             , "READFILE"
             , "TRUNCATE"
             , "COPY"
             , "IF"
             ]

chunkPropNames = [ "present"
                 , "present_full"
                 , "size"
                 , "offset"
                 , "offset_in"
                 ]

chunkArgNames = [ "path"
                , "size"
                , "ref"
                , "c"
                ]

dataSuffixes = [ "kB", "MB", "GB", "TB"
               , "kiB", "MiB", "GiB", "TiB" ]

reservedNames = chunkNames ++ chunkPropNames ++ chunkArgNames ++ dataSuffixes

identLetters = alphaNum <|> oneOf "_"

languageDef =
  emptyDef { PT.commentStart    = ""
           , PT.commentEnd      = ""
           , PT.commentLine     = "#"
           , PT.nestedComments  = False
           , PT.identStart      = letter
           , PT.identLetter     = identLetters
           , PT.reservedNames   = reservedNames
           , PT.reservedOpNames = opNames
           }

lexer = PT.makeTokenParser languageDef

identifier = PT.identifier lexer
reserved   = PT.reserved   lexer
reservedOp = PT.reservedOp lexer
parens     = PT.parens     lexer
braces     = PT.braces     lexer
natural    = PT.natural    lexer
whiteSpace = PT.whiteSpace lexer
brackets   = PT.brackets   lexer


