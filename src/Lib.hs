module Lib where

import qualified Data.ByteString.Lazy as BL
import MkImg.Parse
import MkImg.Generate

mkImg :: String -> BL.ByteString
mkImg = generate . parse

