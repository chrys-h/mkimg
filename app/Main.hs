module Main where

import qualified Data.ByteString.Lazy as BL
import Options.Applicative
import Data.Monoid ((<>))

import Lib (mkImg)

data MIOptions = MIOptions
               { mioOutputFile :: Maybe String
               , mioInputFile  :: String }

optParser = MIOptions <$> optional (strOption (long "output"
                                               <> short 'o'
                                               <> metavar "FILENAME"))
                      <*> strArgument (metavar "FILENAME")

main :: IO ()
main = execParser (info optParser mempty) >>= runWithOpts

runWithOpts :: MIOptions -> IO ()
runWithOpts o = (mkImg <$> input) >>= output
  where input = readFile $ mioInputFile o
        output = case mioOutputFile o of
                   (Just f) -> BL.writeFile f
                   Nothing -> BL.putStr

