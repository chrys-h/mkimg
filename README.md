# mkimg

Mkimg aims at providing a simple and flexible way to generate disk images for use in virtual (or physical) machines. It is meant as an alternative to the usual method of contruction of such images. The usual method, used for instance in hobbyist OS development, relies on loopback devices and the kernel's filesystem facilities, which makes the process error-prone and tedious and requires superuser privilege, which is overkill since the aim is to write a glorified binary file.

## Usage

Mkimg generates binary images based on textual specifications written in a simple DSL. For reference on the DSL, see doc/DSL.md. The command-line interface is straightforward: the specification file's name is given as an argument, and the output file can optionally be specified with the -o <filename> parameter. If the -o parameter is not used, the generated binary will be written to the standard output. It can then be piped into dd or netcat for instance.

## TODO

- [x] Parser for DSL
  - [x] Parsing of chunks
  - [x] Parsing of math expressions
- [ ] Evaluation of expressions
  - [x] Tests
  - [ ] Evaluation of chunk properties
- [ ] Generation of chunks
  - [x] Tests
  - [x] Generation of raw data
  - [x] 'Simple' chunks (IMAGE, CONCAT, etc.)
  - ...
- [x] CLI arguments
- [ ] Use Data.Text instead of String
- [ ] Refactor
- [ ] Variables in expressions
- [ ] Ternary conditions in expressions
- [ ] More sophisticated chunks
  - [ ] Partitions and disk labels (MBR, GPT)
  - [ ] Filesystems
  - [ ] Copying entire directory trees into a filesystem
  - [ ] Loops

