# Mkimg DSL

The DSL used by mkimg is very simple: the image to be generated is divided in chunks, which are units of generation, each chunk having different properties, attributes and a type, which dictate how the chunk is generated.
The syntax to declare a chunk is:

```
CHUNKTYPE (attribute: value, attribute: value) {
	<contents...>
}
```

Each chunk can have a label, that can be used to reference the chunk in other places. The syntax to declare a chunk with a label is:

```
"label" = CHUNK (...) {
    ...
}
```

Labels of the form `__label__` are reseved. The IMAGE chunk (there can only be one) is always labelled `__image__`

## Chunk types

### Image

The image chunk is the equivalent of the `main` function of a C program. There must be exactly one image chunk by specification file, and its contents are the one of the final image. The contents of the image chunk are other chunks, which will be concatenated in the order they are declared in the specification file. An image chunks takes no attributes.
For instance:

```
IMAGE {
	CHUNK { ... }
	OTHERCHUNK { ... }
}
```

### Concat

A concat chunk contains other chunks, which are to be concatenated in the order they are declared. A concat chunk takes no attributes. For instance:


```
CONCAT {
	CHUNK { ... }
	OTHERCHUNK { ... }
}
```

### Literal

A literal chunk is a chunk that contains directly specified data. The data is specified as a block of hexadecimal bytes, which are transferred to binary in the order they are declared in (see "Raw data representation" below). A literal chunk takes no attributes.
For instance:

```
LITERAL {
	4b 89 14 08 49 83 c1 08  4a 8b 04 0b 4b 89 04 08
	49 83 c1 08 4c 39 ce 0f  84 1b fb ff ff 4e 8b 34
	0b 4f 89 34 08 4e 8b 5c  0b 08 4f 89 5c 08 08 4a
	8b 4c 0b 10 4b 89 4c 08  10 4a 8b 7c 0b 18 4b 89
	7c 08 18 4e 8b 54 0b 20  4f 89 54 08 20 4a 8b 54
	0b 28 4b 89 54 08 28 4a  8b 44 0b 30 4b 89 44 08
	30 4e 8b 74 0b 38 4f 89  74 08 38 49 83 c1 40 4c
	39 ce 75 a9 e9 bf fa ff  ff 0f 1f
}
```

Alternatively, a literal chunk can be represented by raw data enclosed in square brackets (`[` and `]`):

```
[55 AA]
```

### ReadFile

A readFile chunk is a chunk that contains all the data contained in a given file. The chunk has no contents and takes a `path` attribute, which is the path of the file, absolute or relative to imggen's working directory. The path needs to be between quotation marks (see "File path representation" below).

```
READFILE(path: "boot_sector.bin")
```

### Truncate

A truncate chunk is a chunk that contains other chunks and takes a `size` argument. The contents of the chunk will be appended and truncated to a maximum size of the value of the `size` argument. The size argument is given as a data size (see "Data size representation" below). If the contents are less than the size of the truncate chunk, they are concatenated and copied "as is".

```
TRUNCATE (size: 446) {
	READFILE (path: "boot_sector.bin")
}
```

### Copy

A copy chunk is a chunk that references another chunk by label and copies the referenced chunk's contents in place and as is. The copy chunk itself has no contents and takes a `ref` as argument, the value being the label of the referenced chunk.

```
IMAGE {
	COPY (ref: "some_chunk")
}

"some_chunk" = LITERAL {
	4b 89 14 08 49 83 c1 08  4a 8b 04 0b 4b 89 04 08
	49 83 c1 08 4c 39 ce 0f  84 1b fb ff ff 4e 8b 34
	0b 4f 89 34 08 4e 8b 5c  0b 08 4f 89 5c 08 08 4a
	8b 4c 0b 10 4b 89 4c 08  10 4a 8b 7c 0b 18 4b 89
	7c 08 18 4e 8b 54 0b 20  4f 89 54
}
```

### If

An if chunk takes an integer and two chunks as contents. If the integer is non-zero, the first chunk is generated in-place. Otherwise, the second chunk is generated in-place. If only one chunk is provided, the second chunks defaults to an empty chunk.

```
IMAGE {
	...
	IF (c: ...) {
		CONCAT { ... }
		[ ... ]
	}
}
```

## Numeric expressions

The DSL supports numeric expressions that can be used anywhere an integer can be used. Those expressions support integers, operations and functions on integers, and integrating properties of chunks. Certain operations interpret intergers as booleans, much in the same way that the C language does: 0 is false, any other value is true.

### Operators and functions

- `a + b`: integer addition
- `a - b`: integer subtraction
- `a * b`: integer multiplication
- `a ** b`: integer exponentiation
- `a / b`: integer division
- `a % b`: integer modulus
- `-a`: integer negation
- `a & b`: bitwise and
- `a | b`: bitwise or
- `a ^ b`: bitwise exclusive or
- `~a`: bitwise not
- `a && b`: boolean and
- `a || b`: boolean or
- `a << b`: bitwise left shift
- `a >> b`: bitwise right shift

- `a == b`: equality
- `a != b`: different
- `a < b`: a lesser than b
- `a <= b`: a lesser than or equal to b
- `a > b`: a greater than b
- `a >= b`: a greater than or equal to b

### Properties of chunks

The syntax to get a property of a chunk is

```
chunk_label.prop(param, param)
```

Quotation marks may be used around the chunk label if it contains spaces or special characters. The parentheses are to be omitted it the property takes no parameters.

The supported properties are:

```
chunk.present
```

1 if the chunk is present in the final result. 0 otherwise. The chunk is considered to be present even if it is truncated.

```
chunk.present_full
```

1 if the chunk is present in full (not truncated) in the final result, 0 otherwise.

```
chunk.size
```

The size in bytes of the chunk.

```
chunk.offset
```

The offset of the chunk in the final result. If the chunk is present multiple times or not present in the result, the program fails with an error.

```
chunk.offset_in(container)
```

The offset of the chunk inside of the container. If the chunk is present multiple times or not present at all in the container, the program fails with an error.


### Variables

TODO

## Data representation

### Raw data representation

In literal chunks, the raw data is represented in one of three ways:

In base64 by enclosing it in square brackets, even if the brackets notation for LITERAL chunks is used:
```
[[
    AjaC86Fpfs8maC5mSzmLPBc12uBl/LcX7WrTybKUyevEBOiu0u3k3ON+Fwx8MJhFUyPEw7HcTqU3
    R78cfv2v7fssLpmDh/QGKS8PDKE37Gn8yvKcsSK4qKZfSo04iLYbV3MvzHg0/f5GEzUePF0YaMmT
    AKnKxF1W117+tBqT43bduCWzbanPJXUHImZdGJdif7sDsGbSi8TNxtLc/RqIetD5qTG5/1CXnK5e
]]
```

As ASCII text in quotaton marks:
```
[ "This is text" ]
```

As a sequence of integers with specifications on how they are represented on the disk:
```
0x55AASul
(2 + 5)Cu
```

An integer may be specified directly, or a formula may be used instead. A formula must be in parentheses.

The possible prefixes are:
- `0x` for hexadecimal
- `0d` for decimal
- `0b` for binary
By default, integers are hexadecimal if not part of a formula.
A formulae cannot be prefixed with a prefix ; integers in formulae are always decimal.

The suffixes specify the length, endianness and signedness of the integer. They must be specified in that order, although some or all may be omitted. Suffixes are case-sensitive.

Length suffixes are:
- `O`: octet, 8 bits (by default)
- `S`: short, 16 bits
- `L`: long, 32 bits
- `LL`: long long, 64 bits
- `LLL`: long long long, 128 bits

Endianness suffixes are:
- `l`: little-endian (default)
- `m`: big endian

Signedness suffixes are:
- `u`: unsigned (by default)
- `s`: signed

### Data size representation

Data sizes are represented by an natural number and a unit. If no unit is specified, the unit is 1 byte. Other supported units are:
- Powers of 1000 bytes: `kB`, `MB`, `GB`, `TB`
- Powers of 1024 bytes: `kiB`, `MiB`, `GiB`, `TiB`

### File path representation




